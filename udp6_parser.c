#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "udp6_parser.h"

#define IPV6_VERSION 6
#define IPV6_DUMMY_FLOW 0x60000000

static uint16_t _compute_udp6_csum(const packet_t *p) {
  const uint32_t udp_size = sizeof(struct udphdr) + p->pl_size;
  const uint16_t transport = IPPROTO_UDP;
  uint32_t sum = 0;

  for (size_t i = 0; i < sizeof(p->ip_addrs); i += 2) {
    uint16_t word = p->ip_addrs[i] << 8;
    word |= p->ip_addrs[i + 1];
    sum += word;
  }
  sum += p->udp.port_src;
  sum += p->udp.port_dst;
  sum += 2 * udp_size;
  sum += transport;
  for (size_t i = 0; i < p->pl_size; i += 2) {
    uint16_t word = p->payload[i] << 8;
    if (i != p->pl_size - 1) // Prevent invalid read if odd payload size
      word |= p->payload[i + 1];
    sum += word;
  }
  while (sum >> 16)
    sum = (sum & 0xFFFF) + (sum >> 16);
  return ~sum;
}

uint8_t *udp6_forge_packet(const packet_t *p, size_t *size) {
  uint8_t *packet;
  struct ip6_hdr *ip6;
  struct udphdr *udp;

  if (!p || !size)
    return NULL;
  *size = sizeof(*ip6) + sizeof(*udp) + p->pl_size;
  if ((packet = malloc(*size)) == NULL)
    return NULL;
  // Forge IPv6 header
  ip6 = (struct ip6_hdr*)packet;
  ip6->ip6_ctlun.ip6_un1.ip6_un1_flow = htonl(IPV6_DUMMY_FLOW);
  ip6->ip6_ctlun.ip6_un1.ip6_un1_plen = htons(*size - sizeof(*ip6));
  ip6->ip6_ctlun.ip6_un1.ip6_un1_nxt = IPPROTO_UDP;
  ip6->ip6_ctlun.ip6_un1.ip6_un1_hlim = 64;
  memcpy(ip6->ip6_src.__in6_u.__u6_addr8, p->ip.addr_src,
         sizeof(p->ip.addr_src));
  memcpy(ip6->ip6_dst.__in6_u.__u6_addr8, p->ip.addr_dst,
         sizeof(p->ip.addr_dst));
  // Forge UDP header
  udp = (struct udphdr*)(ip6 + 1);
  udp->source = htons(p->udp.port_src);
  udp->dest = htons(p->udp.port_dst);
  udp->len = ip6->ip6_ctlun.ip6_un1.ip6_un1_plen;
  udp->check = htons(_compute_udp6_csum(p));
  // Add payload
  memcpy(udp + 1, p->payload, p->pl_size);
  return packet;
}

packet_t *udp6_parse_packet(const uint8_t *data, size_t size) {
  packet_t packet_header;
  packet_t *packet;
  struct iphdr *ip;
  struct ip6_hdr *ip6;
  struct udphdr *udp;

  if (!data)
    return NULL;

  // Parse IPv6 header
  if (size < sizeof(*ip6))
    return NULL;
  ip = (struct iphdr*)data;
  if (ip->version != IPV6_VERSION)
    return NULL;
  ip6 = (struct ip6_hdr*)ip;
  memcpy(packet_header.ip.addr_src, ip6->ip6_src.__in6_u.__u6_addr8,
         sizeof(packet_header.ip.addr_src));
  memcpy(packet_header.ip.addr_dst, ip6->ip6_dst.__in6_u.__u6_addr8,
         sizeof(packet_header.ip.addr_dst));
  data += sizeof(*ip6);
  size -= sizeof(*ip6);

  // Parse UDP header
  if (ip6->ip6_ctlun.ip6_un1.ip6_un1_nxt != IPPROTO_UDP ||
      ntohs(ip6->ip6_ctlun.ip6_un1.ip6_un1_plen) != size)
    return NULL;
  if (size < sizeof(*udp))
    return NULL;
  udp = (struct udphdr*)data;
  if (ntohs(udp->len) != size)
    return NULL;
  packet_header.udp.port_src = ntohs(udp->source);
  packet_header.udp.port_dst = ntohs(udp->dest);
  data += sizeof(*udp);
  size -= sizeof(*udp);

  // Parse Payload
  packet_header.pl_size = size;
  packet = malloc(sizeof(*packet) + size);
  if (!packet)
    return NULL;
  memcpy(packet, &packet_header, sizeof(packet_header));
  memcpy(packet->payload, data, size);
  return packet;
}

static inline void _print_ip(const uint8_t ip[16]) {
  printf("%x%x:%x%x:%x%x:%x%x::",
         ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7]);
  printf("%x%x:%x%x:%x%x:%x%x",
         ip[8], ip[9], ip[10], ip[11], ip[12], ip[13], ip[14], ip[15]);
}

void udp6_print_packet(const packet_t *p) {
  if (!p)
    return;
  printf("UDP6 packet received:\n");
  printf("[");
  _print_ip(p->ip.addr_src);
  printf("]:%d -> [", p->udp.port_src);
  _print_ip(p->ip.addr_dst);
  printf("]:%d\n", p->udp.port_dst);  
  printf("Payload (len=%ld):\n", p->pl_size);
  for (size_t i = 0; i < p->pl_size; ++i)
    printf("%c", p->payload[i]);
}
