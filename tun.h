/**
 * Copyright (C) 2018-2019 ACKLIO SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#pragma once

int tun_create(const char *dev);
