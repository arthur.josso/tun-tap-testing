#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include "tun.h"
#include "udp6_parser.h"

uint8_t *new_response(const packet_t *origin, size_t *size) {
  const char response_payload[] = "OK\n";
  packet_t *p;
  uint8_t *packet;

  p = malloc(sizeof(*p) + sizeof(response_payload) - 1);
  if (!p)
    return NULL;
  memcpy(p->ip.addr_src, origin->ip.addr_dst, sizeof(origin->ip.addr_dst));
  memcpy(p->ip.addr_dst, origin->ip.addr_src, sizeof(origin->ip.addr_src));
  p->udp.port_src = origin->udp.port_dst;
  p->udp.port_dst = origin->udp.port_src;
  p->pl_size = sizeof(response_payload) - 1;
  memcpy(p->payload, response_payload, sizeof(response_payload) - 1);
  packet = udp6_forge_packet(p, size);
  free(p);
  return packet;
}

int main(void) {
  int tun_fd;
  uint8_t buff[2048];
  ssize_t nread;
  ssize_t nwrite;
  packet_t *packet;

  if ((tun_fd = tun_create("tcpapp")) < 0)
    return 1;
  while (1) {
    nread = read(tun_fd, buff, sizeof(buff));
    if(nread < 0) {
      close(tun_fd);
      return 1;
    }
    packet = udp6_parse_packet(buff, nread);
    if (!packet) {
      printf("Parsing error: Not an UDP/IPv6 packet\n");
      continue;
    }
    udp6_print_packet(packet);
    size_t response_size;
    uint8_t *response = new_response(packet, &response_size);
    if (!response)
      return 1;
    if ((nwrite = write(tun_fd, response, response_size)) == -1) {
      printf("%s\n", strerror(errno));
      return 1;
    }
    if (response_size == (size_t)nwrite)
      printf("Reponse has been sent\n");
    else
      printf("Error: Only %ld bytes written\n", nwrite);
    free(response);
    free(packet);
  }
  close(tun_fd);
  return 0;
}
