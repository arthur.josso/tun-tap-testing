/**
 * Copyright (C) 2018-2019 ACKLIO SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#include <linux/if.h>
#include <linux/if_tun.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int tun_create(const char *dev) {
  struct ifreq ifr;
  int fd;
  char *clonedev = "/dev/net/tun";

  if (!dev || !*dev)
    return -1;
  if ((fd = open(clonedev, O_RDWR)) < 0)
    return -1;
  memset(&ifr, 0, sizeof(ifr));
  ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
  strncpy(ifr.ifr_name, dev, IFNAMSIZ);
  if (ioctl(fd, TUNSETIFF, (void*)&ifr) < 0) {
    close(fd);
    return -1;
  }
  if (strncmp(dev, ifr.ifr_name, IFNAMSIZ) != 0) {
    close(fd);
    return -1;
  }
  return fd;
}
