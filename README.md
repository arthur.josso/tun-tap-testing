# How to use
- Run `./add_tun.sh` to create and configure a TUN called "tcpapp"
- Run `./compile.sh` to compile the code
- Run `./tun_tool` to execute the code, now every IPv6 packets are received by the program
- Run `nc -u6 ::2 4242` to connect to a dummy IP address
- Type whatever you want in Netcat
- The `tun_tool` should receive this packet and respond to it with `"OK\n"`
- Once the testing session finished, you can remove the TUN by running `./rm_tun.sh`
