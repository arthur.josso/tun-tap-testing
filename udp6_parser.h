/**
 * Copyright (C) 2018-2019 ACKLIO SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#pragma once

#include <stdint.h>

typedef struct {
  union {
    struct {
      uint8_t addr_src[16];
      uint8_t addr_dst[16];
    } ip;
    uint8_t ip_addrs[32];
  };
  struct {
    uint16_t port_src;
    uint16_t port_dst;
  } udp;
  size_t pl_size;
  uint8_t payload[];
} packet_t;

packet_t *udp6_parse_packet(const uint8_t *data, size_t size);
uint8_t *udp6_forge_packet(const packet_t *p, size_t *size);
void udp6_print_packet(const packet_t *p);
