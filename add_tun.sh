#!/bin/bash

sudo ip tuntap add mode tun name tcpapp
sudo ifconfig tcpapp inet6 add 2001:470:8957:12::1
sudo ifconfig tcpapp up
sudo ip -6 route add default dev tcpapp
